import java.util.Scanner;

public class AplikasiBank {
    public static void main(String[]args) {
        int pilihan;
        int tambah;
        int tarik_tunai;
        int saldo = 100000;
        String nama = "";
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Nama Anda : " + nama);
        nama = input.next();
        while (true) {
            System.out.println("============================");
            System.out.println("|         Menu ATM         |");
            System.out.println("============================");
            System.out.println("| 1. Cek Saldo             |");
            System.out.println("| 2. Simpan Uang           |");
            System.out.println("| 3. Ambil Uang            |");
            System.out.println("| 4. Keluar                |");
            System.out.println("============================");
            System.out.print("Masukkan Pilihan Anda : ");
            pilihan = Integer.parseInt(input.next());

            switch (pilihan) {
                case 1:
                    System.out.println("Nama  : " + nama);
                    System.out.println("Saldo anda adalah : Rp. " + saldo);
                    break;

                case 2:
                    System.out.println("Nama : " + nama);
                    System.out.print("Jumlah yang anda simpan Rp. ");
                    tambah = input.nextInt();
                    saldo += tambah;
                    System.out.println("Saldo anda adalah Rp." + saldo);
                    break;

                case 3:
                    System.out.println("Nama : " +nama);
                    System.out.println("saldo anda sekarang : " + saldo);
                    System.out.print("Jumlah uang yang anda ambil Rp. ");
                    tarik_tunai = input.nextInt();

                    int kurang = saldo - tarik_tunai;
                    if (kurang < 50000) {
                        System.out.println("Saldo Anda Tidak Mencukupi");
                        System.out.println("Saldo Anda adalah " + saldo);
                    } else {
                        saldo = kurang;
                        System.out.println("saldo anda tersisa " + saldo);
                        System.out.println("saldo yang diambil adalah " + tarik_tunai);
                    }
                    break;

                case 4:
                    System.exit(0);
                    break;

                default:
                    System.out.println("Pilihlah (1,2,3,4)");
                    break;
            }
        }
    }
}
